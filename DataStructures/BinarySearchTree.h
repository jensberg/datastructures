#pragma once

#include "Unit.h"
#include "LinkedList.h"


template <class T>
class BinarySearchTree
{
	struct Node
	{
		T value;
		Node* lhs;
		Node* rhs;
	};
public:
	BinarySearchTree(){};
	~BinarySearchTree(){
		Clear();
	};

	void Insert(const T &value)
	{
		if (!m_root)
			CreateRoot(value);
		else
			AddNode(value, m_root);
	}
	void Clear()
	{
		if (m_root)
		{
			ClearNodes(m_root);
			m_root = nullptr;
			std::cout << "Clear: Tree succesfully cleared!" << std::endl;
		}
		else
			std::cout << "Tree is already empty " << std::endl;
	}
	bool Find(const T &value)
	{
		m_isFound = false;
		if (!m_root)
		{
			std::cout << "ERROR: No items in tree..." << std::endl;
			return false;
		}
		else
		{
			FindValue(value, m_root);
			if (m_isFound == true)
				return true;
			else
				return false;
		}
	}

	void Pre_Order_Traversal(LinkedList<T>& list)
	{
		if (!m_root)
		{
			std::cout << "ERROR: No tree found " << std::endl;
			return;
		}
		PreOrderTraversal(m_root, list);
	}
	void In_Order_Traversal(LinkedList<T>& list)
	{
		if (!m_root)
		{
			std::cout << "ERROR: No tree found " << std::endl;
			return;
		}
		InOrderTraversal(m_root, list);
	}
	void Post_Order_Traversal(LinkedList<T>& list)
	{
		if (!m_root)
		{
			std::cout << "ERROR: No tree found " << std::endl;
			return;
		}
		PostOrderTraversal(m_root, list);
	}

	unsigned int Size()
	{
		int num = 0;
		if (!m_root)
			return 0;
		else
			return SizeCount(m_root, num);
	}
private:
	void ClearNodes(Node* node)
	{
		if (node != nullptr)
		{
			ClearNodes(node->lhs);
			ClearNodes(node->rhs);
			delete node;
			node = nullptr;
		}
	}
	void CreateRoot(const T &value)
	{
		m_root = new Node;
		m_root->value = value;
		m_root->lhs = nullptr;
		m_root->rhs = nullptr;
		std::cout << std::endl << "Root added: " << m_root->value << std::endl;
	};
	void AddNode(const T &value, Node* node)
	{
		if (value < node->value)
		{
			if (node->lhs != nullptr)
				AddNode(value, node->lhs);
			else
			{
				node->lhs = new Node;
				node->lhs->value = value;
				node->lhs->lhs = nullptr;
				node->lhs->rhs = nullptr;
			}
		}
		else if (value > node->value)
		{
			if (node->rhs != nullptr)
				AddNode(value, node->rhs);
			else
			{
				node->rhs = new Node;
				node->rhs->value = value;
				node->rhs->rhs = nullptr;
				node->rhs->lhs = nullptr;
			}
		}
		else if (value == node->value)
			return;
		
		
	}
	void FindValue(const T &value, Node* node)
	{
		if (value == node->value)
		{
			m_isFound = true;
			return;
		}
		if (value < node->value)
		{
			if (node->lhs != nullptr)
				FindValue(value, node->lhs);
			else
				return;
		}

		if (value > node->value)
		{
			if (node->rhs != nullptr)
				FindValue(value, node->rhs);
			else
				return;
		}

	}
	void PreOrderTraversal(Node* node, LinkedList<T> &list)
	{
		if (node)
		{
			list.Push_Back(node->value);
			PreOrderTraversal(node->lhs, list);
			PreOrderTraversal(node->rhs, list);
		}
	}
	void InOrderTraversal(Node* node, LinkedList<T> &list)
	{
		if (node)
		{
			InOrderTraversal(node->lhs, list);
			list.Push_Back(node->value);
			InOrderTraversal(node->rhs, list);
		}
	}
	void PostOrderTraversal(Node* node, LinkedList<T> &list)
	{
		if (node)
		{
			PostOrderTraversal(node->lhs, list);
			PostOrderTraversal(node->rhs, list);
			list.Push_Back(node->value);
		}
	}
	int SizeCount(Node* node, int &n)
	{
		if (node)
		{
			n++;
			SizeCount(node->lhs, n);
			SizeCount(node->rhs, n);
		}
		else
			return n;
	}
	Node* m_root;
	bool m_isFound;
};