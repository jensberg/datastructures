#pragma once
#include <iostream>
template <class T>
class LinkedList
{
	struct Node
	{
		T value;
		Node* nextNode;
	};

public:


	LinkedList() {};
	~LinkedList()
	{ 
		Clear(); }

	void Push_Front(const T &value)
	{
		if (!m_root)
		{
			//Create Root if list is empty
			CreateRoot(value);
		}
		else
		{
			Node* temp = new Node;
			temp->value = value;
			temp->nextNode = m_root;
			m_root = temp;
		}
	}
	void Push_Back(const T &value)
	{
		if (!m_root)
		{
			//Create Root if list is empty
			CreateRoot(value);
		}
		else
		{
			Node* temp = nullptr;
			for (temp = m_root; temp->nextNode; temp = temp->nextNode);
			temp->nextNode = new Node;
			temp->nextNode->value = value;
			temp->nextNode->nextNode = nullptr;
		}
	}
	void Pop_Back()
	{
		if (!m_root)
			std::cout << "Error: No items in list..." << std::endl;
		else if (m_root->nextNode)
		{
			Node* temp = nullptr;
			for (temp = m_root; temp->nextNode->nextNode; temp = temp->nextNode);
			delete temp->nextNode;
			temp->nextNode = nullptr;
		}
		
		else
		{
			delete m_root;
			m_root = nullptr;
		}
	}
	void Pop_Front()
	{
		if (!m_root)
			std::cout << "Error: No items in list..." << std::endl;
		else
		{
			Node* temp = nullptr;
			temp = m_root->nextNode;
			delete m_root;
			m_root = temp;
		}
	}
	void Clear()
	{
		if (m_root)
		{
			while (m_root->nextNode)
			{
				Pop_Back();
			}
			delete m_root;
			m_root = nullptr;
		}
			
	}
	unsigned int Size()
	{
		int numElements = 0;
		if (!m_root)
			return NULL;
		else
		{
			Node* temp = nullptr;
			for (temp = m_root; temp; temp = temp->nextNode)
			{
				numElements++;
			}
		}
		return numElements;
	}
	bool Find(const T &value)
	{
		Node* temp = nullptr;
		if (!m_root)
		{
			std::cout << "ERROR: No items in list" << std::endl;
			return false;
		}
		for (temp = m_root; temp; temp = temp->nextNode)
		{
			if (temp->value == value)
			{
				return true;
			}
		}
		std::cout << "ERROR: Unable to find value" << std::endl;
		return false;
	}
	void PrintListValues()
	{
		if (!m_root)
			std::cout << std::endl << "Nothing to print, empty list" << std::endl;
		else
		{
			Node* temp = nullptr;
			for (temp = m_root; temp->nextNode; temp = temp->nextNode)
			{
				std::cout << temp->value << " ";
			}
			std::cout << temp->value << " " << std::endl;
		}
	}

private:
	void CreateRoot(const T &value)
	{
		m_root = new Node;
		m_root->value = value;
		m_root->nextNode = nullptr;
	}
	Node* m_root;
};