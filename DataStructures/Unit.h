
#pragma once

#include <iostream>
#include <string>
#include "LinkedList.h"

template <class T>
bool are_equal(T a, T b)
{
	return a == b;
}

template <class T>
bool verify(T expected, T got, const std::string message)
{
	std::cout << "__________________________________________________" << std::endl;

	if (are_equal(expected, got))
	{
		std::cout << std::endl << "Passed: " << message << std::endl;
		return true;
	}
	std::cout << "Failed! Expected: " << expected << " Got: " << got << " - " << message << std::endl << std::endl;
	return false;
}
template <class T>
bool verify(LinkedList<T>& expected, LinkedList<T>& got, const std::string message)
{
	std::cout << "__________________________________________________" << std::endl;
	std::cout << message << ":" << std::endl << std::endl;
	std::cout << "Expected order: " << std::endl;
	expected.PrintListValues();
	std::cout << "Recieved order: " << std::endl;
	got.PrintListValues();

	return true;
}


