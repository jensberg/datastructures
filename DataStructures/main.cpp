
#include <iostream>
#include <string>
#include "LinkedList.h"
#include "BinarySearchTree.h"
#include "Unit.h"
#include <vld.h>
int main()
{

	//---------------------//
	//  LINKED LIST TESTS  //
	//---------------------//

		LinkedList<int>* list;
		list = new LinkedList<int>;

		//Push_Back()
		list->Push_Back(4);
		list->Push_Back(6);
		list->Push_Back(8);
		verify<bool>(true, list->Find(8), "Push_Back() & Find()");
		list->PrintListValues();

		//Push_Front()
		list->Push_Front(11);
		list->Push_Front(13);
		verify<bool>(true, list->Find(11), "Push_Front()");
		list->PrintListValues();

		//Size()
		verify<int>(5, list->Size(), "Size()");

		//Pop_Front()
		list->Pop_Front();
		verify<int>(4, list->Size(), "Pop_Front()");
		list->PrintListValues();

		//Pop_Back()
		list->Pop_Back();
		verify<int>(3, list->Size(), "Pop_Back()");
		list->PrintListValues();

		//Clear()
		list->Clear();
		verify<int>(0, list->Size(), "Clear()");
		list->PrintListValues();
		delete list;
		list = nullptr;
		

	 ////////////////////////////////
	////    BINARY SEARCH TREE	////
   ////////////////////////////////
		std::cout << std::endl << "Linked List Test Complete!" << std::endl << std::endl << "Starting Binary Search Tree Test..." << std::endl;
		BinarySearchTree<int>* tree;
		tree = new BinarySearchTree < int >;

		//Insert()
		tree->Insert(10);
		tree->Insert(10);
		tree->Insert(10);
		tree->Insert(5);
		tree->Insert(7);
		tree->Insert(34);
		tree->Insert(28);
		tree->Insert(2);
		tree->Insert(20);
		tree->Insert(32);
		verify<bool>(true, tree->Find(7), "Insert()");

		//Find()
		verify<bool>(false, tree->Find(3), "Find()");
		verify<bool>(true, tree->Find(28), "Find()");
		//Size()
		verify<int>(8, tree->Size(), "Size()");

		//Pre Order Traversal
		LinkedList<int> prelist;
		tree->Pre_Order_Traversal(prelist);
		LinkedList<int> expectedPreList;
		expectedPreList.Push_Back(10);
		expectedPreList.Push_Back(5);
		expectedPreList.Push_Back(2);
		expectedPreList.Push_Back(7);
		expectedPreList.Push_Back(34);
		expectedPreList.Push_Back(28);
		expectedPreList.Push_Back(20);
		expectedPreList.Push_Back(32);

		verify(expectedPreList, prelist, "Pre_Order_Traversal()");
		
		//In Order Traversal
		LinkedList<int> inorderlist;
		tree->In_Order_Traversal(inorderlist);
		LinkedList<int> expectedInOrderList;
		expectedInOrderList.Push_Back(2);
		expectedInOrderList.Push_Back(5);
		expectedInOrderList.Push_Back(7);
		expectedInOrderList.Push_Back(10);
		expectedInOrderList.Push_Back(20);
		expectedInOrderList.Push_Back(28);
		expectedInOrderList.Push_Back(32);
		expectedInOrderList.Push_Back(34);
		verify(expectedInOrderList, inorderlist, "In_Order_Traversal()");

		//Post order
		LinkedList<int> postlist;
		tree->Post_Order_Traversal(postlist);
		LinkedList<int> expectedPostList;
		expectedPostList.Push_Back(2);
		expectedPostList.Push_Back(7);
		expectedPostList.Push_Back(5);
		expectedPostList.Push_Back(20);
		expectedPostList.Push_Back(32);
		expectedPostList.Push_Back(28);
		expectedPostList.Push_Back(34);
		expectedPostList.Push_Back(10);
		verify(expectedPostList, postlist, "Post_Order_Traversal()");

		//Clear
		tree->Clear();
		verify<int>(0, tree->Size(), "Clear()");
		delete tree;
		tree = nullptr;
	
		std::cout << std::endl << "Binary Search Tree Test Complete!" << std::endl;
	system("pause");
	return 0;
}

